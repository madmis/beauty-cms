up:
	docker-compose up -d
build:
	docker-compose build
rm:
	docker-compose rm
kill:
	docker-compose kill
logs:
	docker-compose logs -f
exec-php:
	docker-compose exec php bash
ps:
	docker-compose ps
up-dev:
	docker-compose -f docker-compose.dev.yml up -d
