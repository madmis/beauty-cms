#!/usr/bin/env bash
set -eu

ln -fs /usr/share/zoneinfo/UTC /etc/localtime && dpkg-reconfigure -f noninteractive tzdata


#mkdir -p /var/www/web/uploads/{blog,gallery,service,special-offer}

groupmod -g 4444 dialout
groupmod -g 5555 staff
chown -R www-data:www-data /var/www

#setfacl -R -m u:"www-data":rwX /var/www
#setfacl -dR -m u:"www-data":rwX /var/www


rm -f /var/run/rsyslogd.pid
rsyslogd

/usr/bin/supervisord -c /etc/supervisor/supervisord.conf