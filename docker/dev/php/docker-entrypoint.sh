#!/usr/bin/env bash

set -e

mkdir -p /var/www/web/uploads/{blog,gallery,service,special-offer}

setfacl -R -m u:"www-data":rwX /var/www
setfacl -dR -m u:"www-data":rwX /var/www


rm -f /var/run/rsyslogd.pid
rsyslogd

/usr/bin/supervisord -c /etc/supervisor/supervisord.conf