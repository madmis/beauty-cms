<?php

namespace AppBundle\Model;

use AppBundle\Entity\Product;

/**
 * Class CartItem
 * @package AppBundle\Model
 */
class CartItem
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @param Product $product
     * @param int $quantity
     */
    public function __construct(Product $product, int $quantity)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function totalAmount(): float
    {
        return $this->getProduct()->getUnitPrice() * $this->getQuantity();
    }
}
