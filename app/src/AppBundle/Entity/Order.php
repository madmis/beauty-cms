<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="`order`")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 */
class Order implements EntityInterface, StringableInterface
{
    const STATE_NEW = 'new';
    const STATE_CLOSED = 'closed';
    const STATE_CANCELLED = 'cancelled';
    const STATE_PROCESSED = 'processed';
    const STATE_DELIVERED = 'delivered';
    const STATE_PENDING_PAYMENT = 'pending_payment';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="error.name.is_blank")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="error.email.is_blank")
     * @Assert\Email(message="error.email.invalid")
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="error.phone.is_blank")
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=25, nullable=false, options={"default" : "new"}))
     * @var string
     */
    private $state = self::STATE_NEW;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $stateComment;

    /**
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\OrderProduct",
     *     mappedBy="order",
     *     orphanRemoval=true,
     *     cascade={"persist"}
     * )
     * @var OrderProduct[]
     */
    private $orderProducts;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updated;

    public function __construct()
    {
        $this->orderProducts = new ArrayCollection();
        $this->slug = md5(uniqid(time(), true));
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->getId();
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getStateComment(): ?string
    {
        return $this->stateComment;
    }

    /**
     * @param string $stateComment
     */
    public function setStateComment(?string $stateComment): void
    {
        $this->stateComment = $stateComment;
    }

    /**
     * @return OrderProduct[]|Collection
     */
    public function getOrderProducts(): Collection
    {
        return $this->orderProducts;
    }

    /**
     * @param OrderProduct[] $orderProducts
     */
    public function setOrderProducts(array $orderProducts)
    {
        $this->orderProducts = $orderProducts;
    }

    /**
     * @param Product $product
     * @param int $quantity
     */
    public function addOrderProduct(Product $product, int $quantity): void
    {
        $exists = $this->orderProducts->exists(function ($key, OrderProduct $item) use ($product) {
            return $product->getId() === $item->getProduct()->getId();
        });

        if (!$exists) {
            $item = new OrderProduct;
            $item->setOrder($this);
            $item->setProduct($product);
            $item->setQuantity($quantity);
            $this->orderProducts->add($item);
        }
    }
}
