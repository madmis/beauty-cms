<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Table(name="services")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServiceRepository")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Service implements EntityInterface, StringableInterface
{
    /**
     * @ORM\Column(type="integer", options={"autoincrement":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=240)
     * @Assert\NotBlank(message="Please, type service name")
     * @var string
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255, unique=true)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @Assert\NotBlank(message="Please, type service price")
     * @var float
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Please, type service description")
     * @var string
     */
    private $description;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Assert\NotBlank(
     *     message="Please, upload the image: jpeg OR png",
     *     groups={"Create"}
     * )
     * @Assert\Image(
     *     mimeTypes={ "image/png", "image/jpeg", "image/pjpeg" },
     *     mimeTypesMessage = "Please upload a valid image",
     *     detectCorrupted=true,
     *     groups={"Create"}
     * )
     * @Vich\UploadableField(mapping="service_image", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updated;

    /**
     * @ORM\OneToMany(targetEntity="ServiceImage", mappedBy="service", cascade={"persist"}, orphanRemoval=true)
     * @var ServiceImage[]|Collection|ArrayCollection
     */
    private $images;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(?File $image = null): void
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->getName();
    }


    /**
     * @return ServiceImage[]|ArrayCollection|Collection
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * @return ServiceImage[]|ArrayCollection|Collection
     */
    public function getServiceImages(): Collection
    {
        return $this->getImages();
    }

    /**
     * @param ServiceImage[]|ArrayCollection|Collection $images
     */
    public function setImages(ArrayCollection $images)
    {
        $this->images = $images;
    }

    /**
     * @param ServiceImage $image
     */
    public function addImage(ServiceImage $image): void
    {
        if (!$this->images->contains($image)) {
            $image->setService($this);
            $this->images->add($image);
        }
    }

    /**
     * @param ServiceImage $image
     */
    public function removeImage(ServiceImage $image): void
    {
        if ($this->images->contains($image)) {
            $image->setService(null);
            $this->images->removeElement($image);
        }
    }
}
