<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Table(name="gallery_image")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class GalleryImage implements EntityInterface, StringableInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="GalleryCategory", inversedBy="images")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * @var \AppBundle\Entity\GalleryCategory
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Assert\NotBlank(
     *     message="Please, upload the image: jpeg OR png",
     *     groups={"Create"}
     * )
     * @Assert\Image(
     *     mimeTypes={ "image/png", "image/jpeg", "image/pjpeg" },
     *     mimeTypesMessage = "Please upload a valid image",
     *     detectCorrupted=true,
     *     groups={"Create"}
     * )
     * @Vich\UploadableField(mapping="gallery_image", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updated;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }


    /**
     * @return \DateTime
     */
    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(?File $image = null): void
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->getImage();
    }

    /**
     * @return \AppBundle\Entity\GalleryCategory
     */
    public function getCategory(): ?GalleryCategory
    {
        return $this->category;
    }

    /**
     * @param \AppBundle\Entity\GalleryCategory $category
     */
    public function setCategory(GalleryCategory $category)
    {
        $this->category = $category;
    }
}
