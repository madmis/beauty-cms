<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="shop_product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Product implements EntityInterface, StringableInterface
{
    /**
     * @ORM\Column(type="integer", options={"autoincrement":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 150,
     *      minMessage = "Product title should be at least {{ limit }} characters long",
     *      maxMessage = "Product title should not be longer than {{ limit }} characters"
     * )
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"}, unique=true, unique_base="id", updatable=true)
     * @ORM\Column(type="string")
     * @var string
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="products")
     * @ORM\JoinTable(
     *     name="shop_product_category",
     *     joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     * @var \Doctrine\Common\Collections\Collection|\AppBundle\Entity\ProductCategory[]
     */
    private $categories;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     * @var float
     */
    private $price = 0;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     * @var int
     */
    private $quantity = 0;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     * @var string
     */
    private $excerpt;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank(message = "Please enter some description for product")
     * @var string
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="ProductImage", mappedBy="product", cascade={"persist", "remove"}, orphanRemoval=true)
     * @var ProductImage[]|Collection|ArrayCollection
     */
    private $images;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updated;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return ProductCategory[]|Collection|ArrayCollection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * @param ProductCategory[]|Collection|ArrayCollection $categories
     */
    public function setCategories(ArrayCollection $categories): void
    {
        $this->categories = $categories;
    }

    /**
     * @param ProductCategory $category
     */
    public function addCategory(ProductCategory $category): void
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }
    }

    /**
     * @param ProductCategory $category
     */
    public function removeCategory(ProductCategory $category): void
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->getTitle();
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return ProductImage[]|ArrayCollection|Collection
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * @param ProductImage[]|ArrayCollection|Collection $images
     */
    public function setImages(ArrayCollection $images)
    {
        $this->images = $images;
    }

    /**
     * @param ProductImage $image
     */
    public function addImage(ProductImage $image): void
    {
        if (!$this->images->contains($image)) {
            $image->setProduct($this);
            $this->images->add($image);
        }
    }

    /**
     * @param ProductImage $image
     */
    public function removeImage(ProductImage $image): void
    {
        if ($this->images->contains($image)) {
            $image->setProduct(null);
            $this->images->removeElement($image);
        }
    }

    /**
     * @return int
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getExcerpt(): ?string
    {
        return $this->excerpt;
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt(string $excerpt): void
    {
        $this->excerpt = $excerpt;
    }

    /**
     * @return float
     */
    public function getUnitPrice(): float
    {
        $quantity = $this->getQuantity();
        if (!$this->getQuantity()) {
            $quantity = 1;
        }

        return (float)($this->getPrice() / $quantity);
    }
}
