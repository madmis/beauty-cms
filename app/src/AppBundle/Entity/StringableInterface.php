<?php

namespace AppBundle\Entity;

/**
 * Interface StringableInterface
 * @package AppBundle\Entity
 */
interface StringableInterface
{
    /**
     * @return string
     */
    public function __toString(): string;
}
