<?php

namespace AppBundle\Service;

use AppBundle\Entity\Product;
use AppBundle\Model\CartItem;
use AppBundle\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class ShoppingCart
 * @package AppBundle\Service
 */
class ShoppingCart implements ShoppingCartInterface
{
    private const CART_KEY = 'shoppingCart';

    /**
     * @var Session
     */
    private $storage;

    /**
     * @var ProductRepository
     */
    private $repository;

    /**
     * @param Session $storage
     * @param ProductRepository $repository
     */
    public function __construct(Session $storage, ProductRepository $repository)
    {
        $this->storage = $storage;
        $this->repository = $repository;
    }

    /**
     * @param int $productId
     * @param int $quantity
     * @return void
     */
    public function add(int $productId, int $quantity): void
    {
        $cart = $this->storage->get(static::CART_KEY, []);
        $cart[$productId] = $quantity;

        $this->storage->set(static::CART_KEY, $cart);
    }

    /**
     * @param int $productId
     */
    public function remove(int $productId): void
    {
        $cart = $this->storage->get(static::CART_KEY, []);
        unset($cart[$productId]);

        $this->storage->set(static::CART_KEY, $cart);
    }

    /**
     * @param int $productId
     * @return bool
     */
    public function has(int $productId): bool
    {
        $cart = $this->storage->get(static::CART_KEY, []);

        return isset($cart[$productId]);
    }

    /**
     * @return array|CartItem[]
     */
    public function getCartItems(): array
    {
        $cart = $this->storage->get(static::CART_KEY, []);
        if (!$cart) {
            return [];
        }

        /** @var Product[] $products */
        $products = $this->repository->findBy(['id' => array_keys($cart)], ['title' => 'ASC']);
        $items = [];
        foreach ($products as $product) {
            $items[] = new CartItem($product, $cart[$product->getId()]);
        }

        return $items;
    }

    public function clear(): void
    {
        $this->storage->remove(static::CART_KEY);
    }
}
