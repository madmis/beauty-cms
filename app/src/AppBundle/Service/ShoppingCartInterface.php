<?php

namespace AppBundle\Service;

use AppBundle\Model\CartItem;

/**
 * Interface ShoppingCartInterface
 * @package AppBundle\Service
 */
interface ShoppingCartInterface
{
    /**
     * @param int $productId
     * @param int $quantity
     * @return void
     */
    public function add(int $productId, int $quantity): void;

    /**
     * @param int $productId
     * @return void
     */
    public function remove(int $productId): void;

    /**
     * @param int $productId
     * @return bool
     */
    public function has(int $productId): bool;

    /**
     * @return array|CartItem[]
     */
    public function getCartItems(): array;

    public function clear(): void;
}
