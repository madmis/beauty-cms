<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Testimonial;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TestimonialType
 * @package AppBundle\Form\Type
 */
class TestimonialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true,
                'label' => 'form.label.author',
            ])
            ->add('text', TextareaType::class, [
                'attr' => ['rows' => 10],
                'required' => true,
                'label' => 'form.label.comment',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Testimonial::class,
        ]);
    }
}