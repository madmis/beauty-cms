<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrderContactsType
 * @package AppBundle\Form\Type
 */
class OrderContactsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true,],
                'required' => true,
                'label' => 'shop.checkout.name',
            ])
            ->add('email', TextType::class, [
                'required' => true,
                'label' => 'shop.checkout.email_address',
            ])
            ->add('phone', TextType::class, [
                'required' => true,
                'label' => 'shop.checkout.phone_number',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}