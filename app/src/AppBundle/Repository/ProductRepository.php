<?php

namespace AppBundle\Repository;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\Product;
use Doctrine\ORM\EntityRepository;

/**
 * Class ProductRepository
 * @package AppBundle\Repository
 */
class ProductRepository extends EntityRepository implements RepositoryInterface
{
    /**
     * @param \AppBundle\Entity\EntityInterface|\AppBundle\Entity\Product $entity
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(EntityInterface $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    /**
     * @param int $limit
     * @return Product[]
     */
    public function findRecent(int $limit = 5): array
    {
        $qb = $this->createQueryBuilder('p');

        return $qb->where('p.quantity > 0')
            ->addOrderBy('p.created', 'DESC')
            ->getQuery()
            ->setMaxResults($limit)
            ->getResult();
    }


    /**
     * @param \AppBundle\Entity\Product $current
     * @param int $limit
     * @return Product[]
     */
    public function findSimilar(Product $current, int $limit = 3): array
    {
        $qb = $this->createQueryBuilder('p');

        return $qb->where('p.id != :id')
            ->setParameter('id', $current->getId())
            ->addOrderBy('p.created', 'DESC')
            ->getQuery()
            ->setMaxResults($limit)
            ->getResult();
    }
}
