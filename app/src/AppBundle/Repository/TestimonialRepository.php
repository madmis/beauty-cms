<?php

namespace AppBundle\Repository;

use AppBundle\Entity\EntityInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Class TestimonialRepository
 * @package AppBundle\Repository
 */
class TestimonialRepository extends EntityRepository implements RepositoryInterface
{
    /**
     * @param \AppBundle\Entity\EntityInterface|\AppBundle\Entity\Testimonial $entity
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(EntityInterface $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }
}
