<?php

namespace AppBundle\Repository;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\ProductCategory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class ProductCategoryRepository
 * @package AppBundle\Repository
 */
class ProductCategoryRepository extends EntityRepository implements RepositoryInterface
{
    /**
     * @param EntityInterface|ProductCategory $entity
     * @throws OptimisticLockException
     */
    public function save(EntityInterface $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }
}
