<?php

namespace AppBundle\Repository;

use AppBundle\Entity\BlogPost;
use AppBundle\Entity\EntityInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Class BlogPostRepository
 * @package AppBundle\Repository
 */
class BlogPostRepository extends EntityRepository implements RepositoryInterface
{
    /**
     * @param \AppBundle\Entity\EntityInterface|\AppBundle\Entity\BlogPost $entity
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(EntityInterface $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    /**
     * @param \AppBundle\Entity\BlogPost $current
     * @param int $limit
     * @return BlogPost[]
     */
    public function findRecent(BlogPost $current, int $limit = 3): array
    {
        $qb = $this->createQueryBuilder('bp');

        return $qb->where('bp.id != :id')
            ->setParameter('id', $current->getId())
            ->addOrderBy('bp.created', 'DESC')
            ->getQuery()
            ->setMaxResults($limit)
            ->getResult();
    }
}
