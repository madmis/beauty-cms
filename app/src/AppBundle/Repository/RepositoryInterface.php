<?php

namespace AppBundle\Repository;

use AppBundle\Entity\EntityInterface;

/**
 * Interface RepositoryInterface
 * @package AppBundle\Repository
 */
interface RepositoryInterface
{
    /**
     * @param \AppBundle\Entity\EntityInterface $entity
     */
    public function save(EntityInterface $entity): void;
}
