<?php

namespace AppBundle\Repository;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\SpecialOffer;
use Doctrine\ORM\EntityRepository;

/**
 * Class SpecialOfferRepository
 * @package AppBundle\Repository
 */
class SpecialOfferRepository extends EntityRepository implements RepositoryInterface
{
    /**
     * @param EntityInterface|SpecialOffer $entity
     */
    public function save(EntityInterface $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    /**
     * @param SpecialOffer $current
     * @param int $limit
     * @return SpecialOffer[]
     */
    public function findRecent(SpecialOffer $current, int $limit = 3): array
    {
        $qb = $this->createQueryBuilder('bp');

        return $qb->where('bp.id != :id')
            ->setParameter('id', $current->getId())
            ->addOrderBy('bp.created', 'DESC')
            ->getQuery()
            ->setMaxResults($limit)
            ->getResult();
    }

}