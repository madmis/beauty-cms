<?php

namespace AppBundle\Repository;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\Service;
use Doctrine\ORM\EntityRepository;

/**
 * Class ServiceRepository
 * @package AppBundle\Repository
 */
class ServiceRepository extends EntityRepository implements RepositoryInterface
{
    /**
     * @param \AppBundle\Entity\EntityInterface|\AppBundle\Entity\Service $entity
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(EntityInterface $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    /**
     * @param \AppBundle\Entity\Service $current
     * @param int $limit
     * @return Service[]
     */
    public function findRelated(Service $current, int $limit = 3): array
    {
        $qb = $this->createQueryBuilder('s');

        return $qb->where('s.id != :id')
            ->setParameter('id', $current->getId())
            ->getQuery()
            ->setMaxResults($limit)
            ->getResult();
    }
}
