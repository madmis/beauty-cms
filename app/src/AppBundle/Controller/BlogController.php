<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BlogCategory;
use AppBundle\Entity\BlogPost;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/blog")
 *
 * Class BlogController
 * @package AppBundle\Controller
 */
class BlogController extends Controller
{
    /**
     * @Route("/", name="blog")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(): Response
    {
        $posts = $this
            ->get('doctrine')
            ->getRepository(BlogPost::class)
            ->findBy([], ['created' => 'DESC']);

        return $this->render(':blog:list.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/category/{slug}", name="blogCategory")
     *
     * @param \AppBundle\Entity\BlogCategory $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction(BlogCategory $category): Response
    {
        return $this->render(':blog:categoryList.html.twig', [
            'category' => $category,
        ]);
    }

    /**
     * @Route("/{slug}", name="blogPost")
     *
     * @param \AppBundle\Entity\BlogPost $post
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAction(BlogPost $post): Response
    {
        $categories = $this
            ->get('doctrine')
            ->getRepository(BlogCategory::class)
            ->findBy([], ['name' => 'ASC']);
        $recent = $this
            ->get('repository.blog_post')
            ->findRecent($post);

        return $this->render(':blog:post.html.twig', [
            'post' => $post,
            'categories' => $categories,
            'recent' => $recent,
        ]);
    }
}
