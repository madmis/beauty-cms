<?php

namespace AppBundle\Controller\Admin;

use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminController
 * @package AppBundle\Controller\Admin
 */
class AdminController extends BaseAdminController
{
    /**
     * @Route("/upload-file/{type}", name="admin_upload_file")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param string $type
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function uploadFile(Request $request, string $type)
    {
        return new Response();
    }

}
