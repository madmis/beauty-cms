<?php

namespace AppBundle\Controller;

use AppBundle\Entity\GalleryImage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $servicesList = $this->get('repository.service')->findBy([], [], 3);
        $testimonialList = $this->get('repository.testimonial')->findBy([], [], 5);
        $galleryImages = $this
            ->get('doctrine')
            ->getRepository(GalleryImage::class)
            ->findBy([], ['created' => 'DESC'], 8);

        return $this->render('default/index.html.twig', [
            'servicesList' => $servicesList,
            'testimonialList' => $testimonialList,
            'galleryImages' => $galleryImages,
        ]);
    }

    /**
     * @Route("/about", name="about")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function aboutAction()
    {
        return $this->render('default/about.html.twig');
    }

    /**
     * @Route("/contacts", name="contacts")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contactsAction()
    {
        return $this->render('default/contacts.html.twig');
    }
}
