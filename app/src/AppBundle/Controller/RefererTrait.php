<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Trait RefererTrait
 * @package AppBundle\Controller
 */
trait RefererTrait
{
    /**
     * @param Request $request
     * @param string $defaultReferer
     * @return RedirectResponse
     */
    public function redirectToReferer(Request $request, $defaultReferer = null): RedirectResponse
    {
        $referer = $request->headers->get('referer', $defaultReferer);

        return new RedirectResponse($referer);
    }
}