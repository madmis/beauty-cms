<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Order;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductCategory;
use AppBundle\Form\Type\OrderContactsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/shop")
 *
 * Class ServiceController
 * @package AppBundle\Controller
 */
class ShopController extends Controller
{
    use RefererTrait;

    /**
     * @Route("/", name="shopHome")
     * @Route(
     *     "/{viewType}",
     *      name="shopHomeTyped",
     *      defaults={"viewType" = "grid"},
     *      requirements={ "viewType": "grid|list" }
     * )
     *
     * @param string $viewType
     * @return Response
     */
    public function indexAction(string $viewType = 'grid'): Response
    {
        $products = $this->get('repository.product')->findAll();
        $categories = $this->get('repository.product_category')->findAll();
        $recent = $this->get('repository.product')->findRecent();

        return $this->render(':shop:home.html.twig', [
            'products' => $products,
            'total' => count($products),
            'viewType' => $viewType,
            'categories' => $categories,
            'recent' => $recent,
        ]);
    }


    /**
     * @Route("/category/{slug}", name="shopCategory")
     * @Route("/category/{slug}/{viewType}", name="shopCategoryTyped", defaults={"viewType" = "grid"})
     *
     * @param ProductCategory $category
     * @param string $viewType
     * @return Response
     */
    public function categoryAction(ProductCategory $category, string $viewType = 'grid'): Response
    {
        $categories = $this->get('repository.product_category')->findAll();
        $recent = $this->get('repository.product')->findRecent();

        return $this->render(':shop:category.html.twig', [
            'category' => $category,
            'total' => $category->getProducts()->count(),
            'viewType' => $viewType,
            'categories' => $categories,
            'recent' => $recent,
        ]);
    }

    /**
     * @Route("/product/{slug}", name="shopProductView")
     *
     * @param Product $product
     * @return Response
     */
    public function productViewAction(Product $product): Response
    {
        return $this->render(':shop:view.html.twig', [
            'product' => $product,
            'inTheCart' => $this->get('shopping_cart')->has($product->getId())
        ]);
    }

    /**
     * @Route("/cart/add/{slug}", name="shopAddToCart")
     *
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function addToCartAction(Request $request, Product $product): Response
    {
        $this->get('shopping_cart')->add(
            $product->getId(),
            $request->request->getInt('count', $product->getQuantity())
        );
        $this->addFlash('success', 'message.shop.added_to_cart');

        $referer = $this->generateUrl('shopProductView', ['slug' => $product->getSlug()]);

        return $this->redirectToReferer($request, $referer);
    }

    /**
     * @Route("/cart/remove/{id}", name="shopRemoveFromCart")
     *
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function removeFromCartAction(Request $request, Product $product): Response
    {
        $this->get('shopping_cart')->remove($product->getId());

        $referer = $this->generateUrl('shopCartView');

        return $this->redirectToReferer($request, $referer);
    }

    /**
     * @Route("/cart/update", name="shopUpdateCart")
     *
     * @param Request $request
     * @return Response
     */
    public function updateCartAction(Request $request): Response
    {
        $items = $request->request->get('quantity', []);
        if (is_array($items) && $items) {
            foreach ($items as $id => $quantity) {
                $this->get('shopping_cart')->add((int)$id, (int)$quantity);
            }
        }

        $referer = $this->generateUrl('shopCartView');

        return $this->redirectToReferer($request, $referer);
    }

    /**
     * @Route("/cart", name="shopCartView")
     *
     * @return Response
     */
    public function cartViewAction(): Response
    {
        $items = $this->get('shopping_cart')->getCartItems();
        $total = 0;
        foreach ($items as $item) {
            $total += $item->totalAmount();
        }

        return $this->render(':shop:cart.html.twig', [
            'items' => $items,
            'total' => $total,
        ]);
    }

    /**
     * @Route("/checkout", name="shopCheckoutView")
     *
     * @param Request $request
     * @return Response
     */
    public function checkoutAction(Request $request): Response
    {
        $order = new Order();
        $form = $this->createForm(OrderContactsType::class, $order);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $items = $this->get('shopping_cart')->getCartItems();
            foreach ($items as $cartItem) {
                $order->addOrderProduct(
                    $cartItem->getProduct(),
                    $cartItem->getQuantity()
                );
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();

            $this->get('shopping_cart')->clear();

            $trans = $this->get('translator');
            /** @var \Swift_Message $message */
            $message = $this->get('mailer')->createMessage();
            $message
                ->setSubject($trans->trans(
                    'shop.mail.your_order',
                    ['%shop_name%' => $request->getUriForPath('/')]
                ))
                ->setFrom($this->getParameter('mailer_sender'))
                ->setTo($order->getEmail())
                ->setBody(
                    $this->renderView(':shop:_orderReview.html.twig', ['order' => $order]),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            /** @var \Swift_Message $message */
            $message = $this->get('mailer')->createMessage();
            $message
                ->setSubject($trans->trans('shop.mail.new_order'))
                ->setFrom($this->getParameter('mailer_sender'))
                ->setTo($this->getParameter('admin_email'))
                ->setBody(
                    $this->renderView(':shop:_orderReview.html.twig', ['order' => $order]),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            return $this->redirect(
                $this->generateUrl('shopOrderReview', ['slug' => $order->getSlug()])
            );
        }

        $items = $this->get('shopping_cart')->getCartItems();
        $total = 0;
        foreach ($items as $item) {
            $total += $item->totalAmount();
        }

        return $this->render(':shop:checkout.html.twig', [
            'form' => $form->createView(),
            'items' => $items,
            'total' => $total,
        ]);
    }

    /**
     * @Route("/order/{slug}", name="shopOrderReview")
     *
     * @param Order $order
     * @return Response
     */
    public function reviewOrderAction(Order $order): Response
    {
        return $this->render(':shop:orderReview.html.twig', [
            'order' => $order,
        ]);
    }
}
