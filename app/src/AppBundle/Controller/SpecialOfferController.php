<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SpecialOffer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


/**
 * @Route("/special-offers")
 *
 * Class BlogController
 * @package AppBundle\Controller
 */
class SpecialOfferController extends Controller
{
    /**
     * @Route("/", name="specialOffers")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(): Response
    {
        $offers = $this
            ->get('doctrine')
            ->getRepository(SpecialOffer::class)
            ->findBy([], ['created' => 'DESC']);

        return $this->render(':SpecialOffers:list.html.twig', [
            'offers' => $offers,
        ]);
    }


    /**
     * @Route("/{slug}", name="viewSpecialOffer")
     *
     * @param \AppBundle\Entity\SpecialOffer $offer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAction(SpecialOffer $offer): Response
    {
        $recent = $this
            ->get('repository.special_offers')
            ->findRecent($offer);

        return $this->render(':SpecialOffers:offer.html.twig', [
            'offer' => $offer,
            'recent' => $recent,
        ]);
    }

}