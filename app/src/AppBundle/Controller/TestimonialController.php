<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Testimonial;
use AppBundle\Form\Type\TestimonialType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/testimonials")
 *
 * Class TestimonialController
 * @package AppBundle\Controller
 */
class TestimonialController extends Controller
{
    /**
     * @Route("/", name="testimonialsList")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $testimonial = new Testimonial();
        $form = $this->createForm(TestimonialType::class, $testimonial);

        $form->handleRequest($request);

        $captcha = $request->get('g-recaptcha-response');

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$this->captchaVerify($captcha)) {
                $this->addFlash('error', 'message.testimonial.recapthca_invalid');
            } else {
                $em = $this->getDoctrine()->getManager();
                $em->persist($testimonial);
                $em->flush();
                $this->addFlash('success', 'message.testimonial.created_successfully');

                return $this->redirect($this->generateUrl('testimonialsList'));
            }
        }

        return $this->render(':testimonial:list.html.twig', [
            'testimonialList' => $this->get('repository.testimonial')
                ->findBy([], ['created' => 'DESC']),
            'form' => $form->createView(),
        ]);
    }

    private function captchaVerify($recaptcha)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'secret' => '6Lc-sjMUAAAAAGnILNWmRHnuNr65Pa38DFCzYv65',
            'response' => $recaptcha
        ]);
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response)->success;
    }

}
