<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Service;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/service")
 *
 * Class ServiceController
 * @package AppBundle\Controller
 */
class ServiceController extends Controller
{
    /**
     * @Route("/", name="servicesList")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(): Response
    {
        return $this->render(':service:list.html.twig', [
            'servicesList' => $this->get('repository.service')->findAll(),
        ]);
    }

    /**
     * @Route("/{slug}", name="serviceView")
     *
     * @param \AppBundle\Entity\Service $service
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Service $service): Response
    {
        $related = $this
            ->get('repository.service')
            ->findRelated($service);

        return $this->render(':service:view.html.twig', [
            'service' => $service,
            'related' => $related,
        ]);
    }
}
