<?php

namespace AppBundle\Controller;

use AppBundle\Entity\GalleryCategory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/gallery")
 *
 * Class GalleryController
 * @package AppBundle\Controller
 */
class GalleryController extends Controller
{
    /**
     * @Route("/", name="gallery")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(): Response
    {
        $categories = $this
            ->get('doctrine')
            ->getRepository(GalleryCategory::class)
            ->findAll();

        return $this->render(':gallery:list.html.twig', [
            'categories' => $categories,
        ]);
    }
}
