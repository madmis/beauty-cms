Share = {
    vk: function(purl) {
        url  = 'http://vk.com/share.php?';
        url += 'url='          + encodeURIComponent(purl);
        // url += '&title='       + encodeURIComponent(ptitle);
        // url += '&description=' + encodeURIComponent(text);
        // url += '&image='       + encodeURIComponent(pimg);
        url += '&noparse=true';
        Share.popup(url);
    },
    facebook: function(purl) {
        url  = 'https://www.facebook.com/sharer/sharer.php?';
        url += 'u='     + encodeURIComponent(purl);
        // url += 'u='     + encodeURIComponent(ptitle);
        // url += '&p[summary]='   + encodeURIComponent(text);
        // url += '&p[url]='       + encodeURIComponent(purl);
        // url += '&p[images][0]=' + encodeURIComponent(pimg);
        Share.popup(url);
    },
    twitter: function(purl, ptitle) {
        url  = 'http://twitter.com/share?';
        url += 'text='      + encodeURIComponent(ptitle);
        url += '&url='      + encodeURIComponent(purl);
        url += '&counturl=' + encodeURIComponent(purl);
        Share.popup(url);
    },
    popup: function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    }
};