# Beauty CMS

## Template

__://devitems.com/html/beautyhouse-preivew/beautyhouse/elements-feature.html

## Commands
```
$ bin/console c:c
$ bin/console assets:install
$ bin/console doctrine:database:create
$ bin/console doctrine:schema:create
$ bin/console doctrine:schema:update -f
$ bin/console fos:user:create
$ bin/console fos:user:activate username
$ bin/console fos:user:promote username

```

## Liip Imagine cache
```
$ cd web
$ php -d memory_limit=-1 ../bin/console liip:imagine:cache:resolve uploads/products/*.jpg
```

## Themes
Theme defined in parameters.yml.
Theme translations can be defined in theme/translations

## Backup

Copy `var/data/julia.sqlite` and `web/uploads`. Pack them.
```
$ tar -cvzf lesik.pp.ua_06102017.tar.gz ./
```
And copy

```
$ scp user@ip:/home/user/test/lesik.pp.ua_26102017.tar.gz ./
```
